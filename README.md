# demo-symfony

Follow these steps in order to run project on your machine lcaolly

`git clone https://ktanveer@bitbucket.org/ktanveer/demo-symfony.git`

cd demo-symfony

create database in DBMS you are using (DBeaver/SqlBench/phpmyadmin)

rename .env.dist to .env

search DATABASE_URL and set ur database credentials along with the schema name

run `composer update` 

run `php bin/console doctrine:migrations:migrate` 

run `php bin/console server:run`

navigate to `http://127.0.0.1:8000/company`